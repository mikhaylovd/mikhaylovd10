import java.text.*;
import java.util.*;
import java.util.Scanner;

public class Main {
    public static void main(String[] args) throws ParseException {
        SimpleDateFormat ft = new SimpleDateFormat("dd.MM.yyyy");
        Scanner scanner = new Scanner(System.in);
        Date date = null;
        boolean flag = true;
        while (true) {
            flag = true;
            try {
                String str = scanner.nextLine();
                date = ft.parse(str);
            }catch (Exception e){
                System.out.println("Неверный формат даты");
                flag = false;
            }
            if (flag) break;
        }
        Calendar calendar = Calendar.getInstance();
        calendar.setTime(date);
        if (new GregorianCalendar().isLeapYear(calendar.get(Calendar.YEAR))) System.out.println(calendar.get(Calendar.YEAR) + " является високосным");
        else System.out.println(calendar.get(Calendar.YEAR) + " не является високосным");
    }
}
